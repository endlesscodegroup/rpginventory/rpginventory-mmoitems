package ru.endlesscode.rpginventory.mmoitems;

import net.Indyuce.mmoitems.MMOItems;
import net.Indyuce.mmoitems.api.Type;
import net.Indyuce.mmoitems.api.player.PlayerData;
import net.Indyuce.mmoitems.comp.inventory.OrnamentPlayerInventory;
import net.Indyuce.mmoitems.comp.inventory.PlayerInventory;
import net.mmogroup.mmolib.MMOLib;
import net.mmogroup.mmolib.api.item.NBTItem;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import ru.endlesscode.rpginventory.api.InventoryAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

@SuppressWarnings("unused")
public class MmoItemsIntegration extends JavaPlugin {

    @Override
    public void onEnable() {
        PluginManager pm = getServer().getPluginManager();
        if (!pm.isPluginEnabled("MMOItems")) {
            getLogger().severe("Integration not enabled because MMOItems is not enabled.");
        } else if (!pm.isPluginEnabled("RPGInventory")) {
            getLogger().severe("Integration not enabled because RPGInventory is not enabled.");
        } else {
            try {
                tryToEnableIntegration();
                getLogger().info("Integration enabled successfully!");
            } catch (Exception e) {
                getLogger().severe("Something wrong with integration.");
                getLogger().log(Level.WARNING, "Please report the error:", e);
            }
        }
    }

    private void tryToEnableIntegration() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new MmoItemsSyncListener(), this);

        MMOItems mmoItemsPlugin = (MMOItems) Objects.requireNonNull(pm.getPlugin("MMOItems"));
        final boolean iterateWholeInventory = mmoItemsPlugin.getConfig().getBoolean("iterate-whole-inventory");
        mmoItemsPlugin.setPlayerInventory(new RpgInventory(iterateWholeInventory));

        if (iterateWholeInventory) {
            // It registers event listener under the hood
            new OrnamentPlayerInventory();
        }
    }

    private static class MmoItemsSyncListener implements Listener {
        @EventHandler
        public void onRpgInventoryClosed(InventoryCloseEvent event) {
            if (InventoryAPI.isRPGInventory(event.getInventory())) {
                PlayerData.get((OfflinePlayer) event.getPlayer()).updateInventory();
            }
        }
    }

    private static class RpgInventory implements PlayerInventory {
        private final boolean iterateWholeInventory;

        public RpgInventory(boolean iterateWholeInventory) {
            this.iterateWholeInventory = iterateWholeInventory;
        }

        @Override
        public List<EquippedItem> getInventory(Player player) {
            List<EquippedItem> list = new ArrayList<>();

            // Add equipped items in hands
            list.add(new EquippedItem(player.getInventory().getItemInMainHand(), Type.EquipmentSlot.MAIN_HAND));
            list.add(new EquippedItem(player.getInventory().getItemInOffHand(), Type.EquipmentSlot.OFF_HAND));

            // Add equipped armor
            Arrays.stream(player.getInventory().getArmorContents())
                    .filter(Objects::nonNull)
                    .map(itemStack -> new EquippedItem(itemStack, Type.EquipmentSlot.ARMOR))
                    .forEach(list::add);

            // Add equipped passive items from RPGInventory
            InventoryAPI.getPassiveItems(player).stream()
                    .map(itemStack -> new EquippedItem(itemStack, Type.EquipmentSlot.ANY))
                    .forEach(list::add);

            if (this.iterateWholeInventory) {
                Arrays.stream(player.getInventory().getContents())
                        .filter(Objects::nonNull)
                        .map(MMOLib.plugin.getNMS()::getNBTItem)
                        .filter(NBTItem::hasType)
                        .filter(nbtItem -> nbtItem.getType().getEquipmentType() == Type.EquipmentSlot.ANY)
                        .map(nbtItem -> new EquippedItem(nbtItem, Type.EquipmentSlot.ANY))
                        .forEach(list::add);
            }

            return list;
        }
    }
}
