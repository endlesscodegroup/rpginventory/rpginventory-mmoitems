import org.gradle.api.Project
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.jvm.tasks.Jar
import org.gradle.kotlin.dsl.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

private const val JAVA_8 = "1.8"

/** Default project configurations with Kotlin. */
fun Project.configureKotlinProject() {
    configureProject()

    apply(plugin = "kotlin")

    tasks.withType(KotlinCompile::class) {
        kotlinOptions {
            jvmTarget = JAVA_8
            apiVersion = "1.3"
            languageVersion = "1.3"
            javaParameters = true
        }
    }

    dependencies {
        "implementation"(kotlin("stdlib-jdk8"))
    }
}

/** Default project configurations. */
fun Project.configureProject() {
    tasks.withType(JavaCompile::class) {
        sourceCompatibility = JAVA_8
        targetCompatibility = JAVA_8
        options.encoding = "UTF-8"
    }

    repositories {
        jcenter()
    }
}

/** Default publication configurations. */
fun Project.configurePublish() {
    apply(plugin = "maven-publish")

    val sourceJar = tasks.register<Jar>("sourceJar") {
        archiveClassifier.set("sources")
        from(project.the<SourceSetContainer>()["main"].allJava)
    }

    configure<PublishingExtension> {
        publications {
            create<MavenPublication>(project.name) {
                from(components["java"])
                artifact(sourceJar.get())
            }
        }
    }
}
