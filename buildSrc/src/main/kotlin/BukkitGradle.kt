import groovy.lang.Closure
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.delegateClosureOf
import ru.endlesscode.bukkitgradle.extension.Bukkit
import ru.endlesscode.bukkitgradle.extension.RunConfiguration
import ru.endlesscode.bukkitgradle.meta.PluginMeta

// Utils to make BukkitGradle kotlin-friendly

val DependencyHandlerScope.bukkit: ExternalModuleDependency
    get() = (extensions.extraProperties["bukkit"] as Closure<*>).call() as ExternalModuleDependency

fun Bukkit.meta(configure: PluginMeta.() -> Unit) {
    meta(delegateClosureOf(configure))
}

fun Bukkit.run(configure: RunConfiguration.() -> Unit) {
    run(delegateClosureOf(configure))
}
