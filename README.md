# RPGInventory & MMOItems integration

Adds support of RPGInventory slots to MMOItems.

## Requirements

[RPGInventory](https://www.spigotmc.org/resources/11809/) and [MMOItems](https://www.spigotmc.org/resources/39267/) of course.

## How to use?

1. Just copy the `.jar` to `plugins/`.
2. Enjoy!
