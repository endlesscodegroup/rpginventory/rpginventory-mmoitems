plugins {
    id("com.github.ben-manes.versions") version "0.27.0"
    id("ru.endlesscode.bukkitgradle")
}

configureProject()

group = "ru.endlesscode.rpginventory"
version = "0.1"
description = "Insert description of plugin here"

bukkit {
    version = "1.15.2"

    meta {
        setMain("${project.group}.mmoitems.MmoItemsIntegration")
        setName("RPGInventoryMMOItems")
    }

    run {
        setCore("spigot")
        eula = true
    }
}

repositories {
    flatDir { dirs("libs") }
    mavenLocal()
    maven(url = "http://mvn.lumine.io/repository/maven-public/")
}

dependencies {
    compileOnly(bukkit) { isTransitive = false }
    compileOnly("ru.endlesscode.rpginventory:rpginventory:2.3.2")
    compileOnly("net.Indyuce:MMOItems:5.3.2") { isTransitive = false }
    compileOnly("net.mmogroup:mmolib:1.0.6")
}
